package by.courses.students;

import by.courses.students.entity.DisciplineEntity;
import by.courses.students.entity.GradeBookEntity;
import by.courses.students.entity.GroupEntity;
import by.courses.students.entity.StudentEntity;
import by.courses.students.repositories.DisciplineRepository;
import by.courses.students.repositories.GradeBookRepository;
import by.courses.students.repositories.GroupRepository;
import by.courses.students.repositories.StudentRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class StudentsApplication{

    public static void main(String[] args) {
        SpringApplication.run(StudentsApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(StudentRepository studentRepository, GroupRepository groupRepository,
                                  GradeBookRepository gradeBookRepository, DisciplineRepository disciplineRepository){
        return (args) -> {
            groupRepository.save(new GroupEntity(5));
            groupRepository.save(new GroupEntity(1));

            disciplineRepository.save(new DisciplineEntity("Math"));
            disciplineRepository.save(new DisciplineEntity("PE"));
            disciplineRepository.save(new DisciplineEntity("Chemistry"));

            gradeBookRepository.save(new GradeBookEntity(1723450));
            gradeBookRepository.save(new GradeBookEntity(1529402));

            List<DisciplineEntity> disciplineEntities = new ArrayList<>();
            disciplineEntities.add(disciplineRepository.findByName("Math"));
            disciplineEntities.add(disciplineRepository.findByName("Chemistry"));

            studentRepository.save(new StudentEntity("Max", "Johson", groupRepository.findByGroup(5),
                    gradeBookRepository.findByGradebook(1723450), disciplineEntities));

            disciplineEntities.add(disciplineRepository.findByName("PE"));
            studentRepository.save(new StudentEntity("Peter", "Pen", groupRepository.findByGroup(1),
                    gradeBookRepository.findByGradebook(1529402), disciplineEntities));

        };
    }
}
