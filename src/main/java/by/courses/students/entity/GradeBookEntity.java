package by.courses.students.entity;

import javax.persistence.*;

@Entity
@Table(name = "gradebook_table")
public class GradeBookEntity {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "gradebook_number")
    private Integer gradebook;

    @OneToOne(mappedBy = "gradeBook", cascade = CascadeType.ALL)
    private StudentEntity studentEntity;

    public GradeBookEntity(){}

    public GradeBookEntity(Integer gradebook) {
        this.gradebook = gradebook;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGradebook() {
        return gradebook;
    }

    public void setGradebook(Integer gradebook) {
        this.gradebook = gradebook;
    }

    public StudentEntity getStudentEntity() {
        return studentEntity;
    }

    public void setStudentEntity(StudentEntity studentEntity) {
        this.studentEntity = studentEntity;
    }
}
