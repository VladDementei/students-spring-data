package by.courses.students.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "student_table")
public class StudentEntity {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String surname;

    @Column(name = "surname")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_table_id")
    private GroupEntity group;

    //@MapsId
    @OneToOne
    @JoinColumn(name = "gradebook_table_id")
    private GradeBookEntity gradeBook;

    @ManyToMany
    @JoinTable(name = "student_discipline_table",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "discipline_id"))
    private List<DisciplineEntity> disciplines = new ArrayList<>();

    public StudentEntity(){}

    public StudentEntity(String name, String surname, GroupEntity group, GradeBookEntity gradeBook, List<DisciplineEntity> disciplines) {
        this.name = name;
        this.surname = surname;
        this.group = group;
        this.gradeBook = gradeBook;
        this.disciplines = disciplines;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public GroupEntity getGroup() {
        return group;
    }

    public void setGroup(GroupEntity group) {
        this.group = group;
    }

    public GradeBookEntity getGradeBook() {
        return gradeBook;
    }

    public void setGradeBook(GradeBookEntity gradeBook) {
        this.gradeBook = gradeBook;
    }

    public List<DisciplineEntity> getDisciplines() {
        return this.disciplines;
    }
    public void setDisciplines(List<DisciplineEntity> disciplines) {
        this.disciplines = disciplines;
    }
}

