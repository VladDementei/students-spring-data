package by.courses.students.repositories;

import by.courses.students.entity.DisciplineEntity;
import org.springframework.data.repository.CrudRepository;

public interface DisciplineRepository extends CrudRepository<DisciplineEntity, Integer> {
    DisciplineEntity findByName(String name);
}
