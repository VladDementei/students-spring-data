package by.courses.students.repositories;

import by.courses.students.entity.GradeBookEntity;
import org.springframework.data.repository.CrudRepository;

public interface GradeBookRepository extends CrudRepository<GradeBookEntity, Integer> {
    GradeBookEntity findByGradebook(Integer gradebook);
}
