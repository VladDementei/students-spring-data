package by.courses.students.repositories;

import by.courses.students.entity.GroupEntity;
import org.springframework.data.repository.CrudRepository;

public interface GroupRepository extends CrudRepository<GroupEntity, Integer> {
    GroupEntity findByGroup(Integer group);
}
