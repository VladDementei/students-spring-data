package by.courses.students.repositories;

import by.courses.students.entity.StudentEntity;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<StudentEntity, Integer> {
    StudentEntity findByNameAndSurname(String name, String surname);
}
